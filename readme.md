# Color Selector
https://gitlab.com/jadermcs/color-selector

Compiling:
```bash
mkdir build && cd build
cmake ..
make
```

Running:
```bash
./DisplayImage ../data/lena.png
./DisplayVideo ../data/sample.avi
./DisplayCam
```