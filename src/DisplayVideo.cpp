#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
typedef cv::Point3_<uint8_t> Pixel;

int blue, green, red;
int rgb = true;
cv::Mat frame, frame0;

void maxEuclidDist(Pixel &pixel, int x, int y, int z)
{
    const int tolerance = 30;
    if ((pow((pixel.x - x),2) + pow((pixel.y - y),2) + pow((pixel.z - z),2)) <= tolerance*tolerance){
        pixel.x = 0;
        pixel.y = 0;
        pixel.z = 255;
    }
    else
        return;
}

static void onClick(int event, int x, int y, int flag, void* param)
{
    if (event != cv::EVENT_LBUTTONDOWN)
        return;

    cout << "XY: " << x << "x" << y << "\t";
    if (rgb) {
        cv::Point3_<uint8_t> color = frame.at<Pixel>(y,x);
        blue = color.x;
        green = color.y;
        red = color.z;
        cout << "RGB: (" << red;
        cout << ", " << green;
        cout << ", " << blue << ")\n";
    }
    else {
        cv::Scalar intensity = frame.at<uchar>(y, x);
        int scale = intensity.val[0];
        cout << "SCALE: " << scale << endl;
        cv::cvtColor(frame, frame0, cv::COLOR_GRAY2BGR);
        frame0.forEach<Pixel>
        ([scale](Pixel &pixel, const int* position) ->
            void {
                if (abs(scale - pixel.x) <= 13)
                {
                    pixel.x = 0;
                    pixel.y = 0;
                    pixel.z = 255;
                }
            });
        cv::imshow("image", frame0);
    }
}

int main(int argc, char** argv)
{
    if ( argc != 2 )
    {
        cout << "usage: DisplayVideo <image_path>" << endl;
        return -1;
    }
    
    cv::VideoCapture cap(argv[1]);

    if(!cap.isOpened())
    {
        cout << "Error opening video stream or file" << endl;
        return -1;
    }

    cv::namedWindow("image", cv::WINDOW_AUTOSIZE);
    cv::setWindowTitle("image", "Color Selector");
    cv::setMouseCallback("image", onClick);

    for(;;)
    {
        cap >> frame;

        if (frame.empty())
            break;
        
        frame0 = frame.clone();
        frame0.forEach<Pixel>
        ([](Pixel &pixel, const int* position) ->
            void {maxEuclidDist(pixel, blue, green, red);});
        cv::imshow("image", frame0);
        char c = (char)cv::waitKey(25);
        if(c==27)
            break;
    }

    cap.release();
    return 0;
}
