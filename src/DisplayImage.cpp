#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
typedef cv::Point3_<uint8_t> Pixel;

cv::Mat image, image0;
int rgb = true;

void maxEuclidDist(Pixel &pixel, int x, int y, int z)
{
    if ((pow((pixel.x - x),2) + pow((pixel.y - y),2) + pow((pixel.z - z),2)) <= 13*13){
        pixel.x = 0;
        pixel.y = 0;
        pixel.z = 255;
    }
    else
        return;
}

static void onClick(int event, int x, int y, int flag, void* param)
{
    if (event != cv::EVENT_LBUTTONDOWN)
        return;

    image0 = image.clone();

    cout << "XY: " << x << "x" << y << "\t";
    if (rgb) {
        cv::Point3_<uint8_t> color = image.at<Pixel>(y,x);
        int blue = color.x;
        int green = color.y;
        int red = color.z;
        cout << "RGB: (" << red;
        cout << ", " << green;
        cout << ", " << blue << ")\n";
        image0.forEach<Pixel>
        ([blue, green, red](Pixel &pixel, const int* position) ->
            void {maxEuclidDist(pixel, blue, green, red);});
        cv::imshow("image", image0);
    }
    else {
        cv::Scalar intensity = image.at<uchar>(y, x);
        int scale = intensity.val[0];
        cout << "SCALE: " << scale << endl;
        cv::cvtColor(image, image0, cv::COLOR_GRAY2BGR);
        image0.forEach<Pixel>
        ([scale](Pixel &pixel, const int* position) ->
            void {
                if (abs(scale - pixel.x) <= 13)
                {
                    pixel.x = 0;
                    pixel.y = 0;
                    pixel.z = 255;
                }
            });
        cv::imshow("image", image0);
    }
}

int main(int argc, char** argv)
{
    if ( argc != 2 )
    {
        cout << "usage: DisplayImage <image_path>" << endl;
        return -1;
    }

    image = cv::imread(argv[1], rgb);

    if (!image.data)
    {
        printf("No image data \n");
        return -1;
    }

    image0 = image.clone();

    cv::namedWindow("image", cv::WINDOW_AUTOSIZE);
    cv::setWindowTitle("image", "Color Selector");
    cv::setMouseCallback("image", onClick);
    cv::imshow("image", image0);
    cv::waitKey(0);
    return 0;
}
